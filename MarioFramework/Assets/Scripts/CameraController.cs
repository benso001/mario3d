﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {
    public GameObject Player;
    public Vector3 Offset;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        transform.position = Player.transform.position + Offset;
	}
}
